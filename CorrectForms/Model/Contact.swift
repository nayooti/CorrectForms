//
//  Contact.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 28.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import Foundation
import Contacts

class Contact {
    var Vorname: String?
    var Nachname: String?
    var Beruf: String?
    var Unternehmen: String?
	var Abteilung: String?
    var Straße: String?
    var PLZ: String?
    var Stadt: String?
    var Land: String?
    var Telefonnummer: String?
    var Mobil: String?
    var Email: String?
    var Webseite: String?
    
    
    var properties: [Int: (String,String?)] {
        var dict: [Int:(String,String?)] = [:]
        let mirror = Mirror(reflecting: self)
        for (index, attr) in mirror.children.enumerated() {
            if let propertyName = attr.label as String? {
                dict[index] = (propertyName, attr.value as? String)
            }
        }
        return dict
    }
    
	init(Vorname: String?, Nachname: String?, Beruf: String?, Unternehmen: String?, Abteilung: String?, Straße: String, PLZ: String?, Stadt: String?, Land: String?, Telefonnummer: String?, Mobil: String?, Email: String?, Webseite: String?) {
        self.Vorname = Vorname
        self.Nachname = Nachname
        self.Beruf = Beruf
        self.Unternehmen = Unternehmen
		self.Abteilung = Abteilung
        self.Straße = Straße
        self.PLZ = PLZ
        self.Stadt = Stadt
        self.Land = Land
        self.Telefonnummer = Telefonnummer
        self.Mobil = Mobil
        self.Email = Email
        self.Webseite = Webseite
        
    }
    
    func update(key: String, value: String?) {
        switch key {
        case "Vorname": Vorname = value
        case "Nachname": Nachname = value
        case "Beruf": Beruf = value
        case "Unternehmen": Unternehmen = value 
        case "Straße": Straße = value
        case "PLZ": PLZ = value
        case "Stadt": Stadt = value
        case "Land": Land = value
        case "Telefonnummer": Telefonnummer = value
        case "Mobil": Mobil = value
        case "Email": Email = value
        case "Webseite": Webseite = value
        default:
            break
        }
        
        print(self.properties)
    }
    
    func makeDeviceContact() -> CNContact {
        let newDeviceContact = CNMutableContact()
        
        newDeviceContact.givenName = self.Vorname ?? ""
        newDeviceContact.familyName = self.Nachname ?? ""
        
        return newDeviceContact
    }
    
    
}


/*
extension Contact: Equatable {
    static func == (lhs: Contact, rhs: Contact) -> Bool {
        
        for lhsProp in lhs.properties {
            if rhs.properties[lhsProp.key]! != lhsProp.value {
                return false
            }
        }
        return true
    }
}

*/

