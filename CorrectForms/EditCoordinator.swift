//
//  EditCoordinator.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 16.07.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class EditCoordinator {
    
    weak var editingTextField: InputTextField?
    weak var grid: GridCollection?
    weak var editSession: EditSession?
    var backupText: String?
    
    func startEditSession(cell: ContactTableCell, with text: String) {
        self.editingTextField = cell.textView
        self.editingTextField?.returnKeyType = UIReturnKeyType.done
        editingTextField?.startEditSession(with: text)
        grid?.update(text: text)
        editSession = cell
        self.backupText = text
    }
    
    func update(with text: String) {
        editingTextField?.startEditSession(with: text)
        grid?.update(text: text)
    }
    
    func cancelEdit() {
        self.editSession?.editingFinished(with: self.backupText ?? "", animated: false)
    }
    /*
    func restoreEdit() {
        
    }
    */
    
}



extension EditCoordinator: CanHandleGridEvents {
    func gridUpdated(with text: String) {
        editingTextField?.text = text
    }
    
    func gridRangeSelected(from start: Int?, to end: Int?) {
        editingTextField?.selectRange(from: start, to: end)
    }
}

extension EditCoordinator: CanHandleTextFieldEvents {
    func handleInput(range: NSRange, replacementString string: String) {
        grid?.handle(range: range, replacementString: string)
    }
    
    func textViewRangeSelected(from start: Int?, to end: Int?) {
        grid?.selectRange(from: start, to: end)
    }
    
    func textViewUpdated(with text: String) {
        grid?.update(text: text)
    }
    
    func textViewEditingFinished(with text: String, animated: Bool) {
        self.editSession?.editingFinished(with: text, animated: animated)
    }
    
    func textViewDeleteEventOnEmptyField() {
        editSession?.handleDeleteEvent()
    }
    
    func getBackupText() -> String {
        return backupText ?? ""
    }
    
    
}
