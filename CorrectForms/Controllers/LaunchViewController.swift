//
//  LaunchViewController.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 14.08.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    private lazy var selectedContact: Contact? = {
        return contacts[selectedContactIndex.row]
        
    }()
    
    var client: APIClient! {
        didSet {
            selectedContact = client.getContactList()[0]
        }
    }
    
    private var contacts:[Contact] {
        return client.getContactList()
    }
    
    private var selectedContactIndex: IndexPath = IndexPath(item: 0, section: 0) {
        didSet {
            print("Selected: \(selectedContactIndex.row)")
        }
    }
    
    lazy var contactSelectionTable: UITableView = {
        var tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "ContactSelectionCell")
        tableView.makeBorder(color: UIColor.gray, width: 0.5)
        return tableView
    }()

    var modeSegmentSelector: UISegmentedControl = {
        var segmentControl = UISegmentedControl(items: ["Plain", "Edit & Drag"])
        segmentControl.selectedSegmentIndex = 0
        segmentControl.addTarget(self, action: #selector(segmentControlSelected), for: .valueChanged)
        return segmentControl
    }()
    
    var tutorialSwitch: UISwitch = {
        var switcher = UISwitch()
        switcher.onTintColor = SystemColor.yellow.uiColor
        switcher.isOn = true 
        return switcher
    }()
    
    var tutorialLabel: UILabel = {
        var label = UILabel()
        label.text = "Einführung"
        return label
    }()
    
    var startButton: UIButton = {
       var button = UIButton()
        button.setTitle("Start Scan", for: .normal)
        button.backgroundColor = SystemColor.tealBlue.uiColor
        button.tintColor = UIColor.white
        button.titleLabel?.textColor = UIColor.white
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(startButtonPressed), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        
        self.title = "Prepare Scan-Simulation"
        
        super.viewDidLoad()

        view.addSubview(contactSelectionTable)
        view.addSubview(modeSegmentSelector)
        view.addSubview(startButton)
        view.addSubview(tutorialLabel)
        view.addSubview(tutorialSwitch)
        
        modeSegmentSelector.translatesAutoresizingMaskIntoConstraints = false
        contactSelectionTable.translatesAutoresizingMaskIntoConstraints = false
        startButton.translatesAutoresizingMaskIntoConstraints = false
        tutorialSwitch.translatesAutoresizingMaskIntoConstraints = false
        tutorialLabel.translatesAutoresizingMaskIntoConstraints = false
        
        contactSelectionTable.leadingAnchor.constraint(equalTo: startButton.leadingAnchor).isActive = true
        contactSelectionTable.trailingAnchor.constraint(equalTo: startButton.trailingAnchor).isActive = true
        
        modeSegmentSelector.leadingAnchor.constraint(equalTo: startButton.leadingAnchor).isActive = true
        modeSegmentSelector.trailingAnchor.constraint(equalTo: startButton.trailingAnchor).isActive = true
        
        startButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        startButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        startButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8).isActive = true
        startButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        // tutorialLabel.bottomAnchor.constraint(equalTo: tutorialSwitch.bottomAnchor).isActive = true
        tutorialLabel.leadingAnchor.constraint(equalTo: startButton.leadingAnchor).isActive = true
        tutorialLabel.topAnchor.constraint(equalTo: contactSelectionTable.bottomAnchor, constant: 20).isActive = true
        
        tutorialSwitch.trailingAnchor.constraint(equalTo: startButton.trailingAnchor, constant: -5).isActive = true
        tutorialSwitch.centerYAnchor.constraint(equalTo: tutorialLabel.centerYAnchor, constant: 0).isActive = true
        
        modeSegmentSelector.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        modeSegmentSelector.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        contactSelectionTable.topAnchor.constraint(equalTo: modeSegmentSelector.bottomAnchor, constant: 15).isActive = true
        // contactSelectionTable.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        contactSelectionTable.bottomAnchor.constraint(equalTo: tutorialLabel.topAnchor, constant: -20).isActive = true
        startButton.topAnchor.constraint(equalTo: tutorialLabel.bottomAnchor, constant: 20).isActive = true
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func segmentControlSelected() {
        if !tutorialSwitch.isOn {
            tutorialSwitch.setOn(true, animated: true)
        }
    }
    
    @objc func startButtonPressed() {
        
        guard let contact = selectedContact else {
            return
        }
      
        var nav: UINavigationController
        let imagePath: String
        if selectedContactIndex == IndexPath(item: 3, section: 0) {
            imagePath = "Hansen.jpg"
        } else if selectedContactIndex == IndexPath(item: 4, section: 0) {
            imagePath = "Hansen.jpg"    // replace by appleseed card
        } else {
            imagePath = "\(contact.Nachname!).jpg"
        }
        
        print(imagePath)
        
        var tutorial: TutorialController
        
        if modeSegmentSelector.selectedSegmentIndex == 0 {
            let controller = PlainViewController(client: self.client, contact: contact)
            controller.imagePath = imagePath
            tutorial = TutorialController(data: [TutorialData(filename: "tut_plain_cursor.jpg"), TutorialData(filename: "tut_plain_range.jpg")])
            nav = UINavigationController(rootViewController: controller)
        } else {
            let controller = ContactViewController(client: self.client, contact: contact)
            controller.imagePath = imagePath
            tutorial = TutorialController(data: [TutorialData(filename: "edit_start_end.jpg"),TutorialData(filename: "tut_drag.jpg"),TutorialData(filename: "tut_range.jpg"), TutorialData(filename: "tut_cursor.jpg")])
            nav = UINavigationController(rootViewController: controller )
        }
       
        
        if self.tutorialSwitch.isOn {
            nav.pushViewController(tutorial, animated: false)
        }
        
        
        self.navigationController?.present(nav, animated: true) {
        
        }
      
    
    }
}

extension LaunchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "ContactSelectionCell")
        
        cell.textLabel?.text = "\(contacts[indexPath.row].Vorname!) \(contacts[indexPath.row].Nachname!)"
        cell.selectionStyle = .none
        if selectedContactIndex == indexPath {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        //cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: self.selectedContactIndex)!.accessoryType = .none
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        self.selectedContactIndex = indexPath
        self.selectedContact = contacts[indexPath.row]
    }
}





