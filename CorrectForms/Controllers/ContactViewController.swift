    //
    //  ViewController.swift
    //  CorrectForms
    //
    //  Created by Bastian van de Wetering on 28.06.18.
    //  Copyright © 2018 Bastian van de Wetering. All rights reserved.
    //
    
    import UIKit
    import Contacts
    import ContactsUI
    
    enum ContactState {
        case none
        case correctionMode
        case activeEdit
        case activeDrag
    }
    
    
    
    class ContactViewController: UIViewController {
        
        
        
        var client: APIClient
        var contact: Contact
        var imagePath: String?
        
        private var inputFieldHeight: CGFloat = Sizes.gridCollectionHeight + 4
        
        var editingCell: ContactTableCell?
        weak var inputListener: CanHandleInput?
        
        var editCoordinator: EditCoordinator
        
        // var correctionMode: Bool = false
        var status: ContactState = .none {
            didSet {
                
                if status == .none {
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                } else {
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                    
                }
            }
        }
        
        lazy var segmentControl: UISegmentedControl = {
            let control = UISegmentedControl(items: ["Ansicht","Bearbeiten"])
            control.selectedSegmentIndex = 0
            control.addTarget(self, action: #selector(segmentControlSelected), for: .valueChanged)
            return control
        }()
        
        lazy var contactTable: ContactTableView = {
            let tableView = ContactTableView()
            // tableView.register(ContactTableCell.self, forCellReuseIdentifier: "ContactTableCell")
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = UITableViewAutomaticDimension
            tableView.separatorStyle = .singleLine
            tableView.dataSource = self
            tableView.delegate = self
            tableView.allowsSelection = false
            return tableView
        }()
        
        
        lazy var gridView: GridView = {
            let view = GridView(editCoordinator: self.editCoordinator)
            return view
        }()
        
        lazy var scanSimulation: ScanSimulation = {
            let scanView = ScanSimulation()
            scanView.setupSubviews(imagePath: self.imagePath!)
            return scanView
        }()
        
        var gridViewBottomAnchor: NSLayoutConstraint!
        
        var cells:[String:ContactTableCell] = [:]
        
        init(client: APIClient, contact: Contact) {
            self.client = client
            self.contact = client.getContact(index: 0)
            self.editCoordinator = EditCoordinator()
            
            
            
            super.init(nibName: nil, bundle: nil)
            
            for prop in contact.properties {
                let key = prop.value.0
                let value = prop.value.1
                let cell = ContactTableCell(textFieldDelegate: self)
                
                cell.key = key
                cell.value = value
                cell.contactTableDelegate = self
                cell.editCoordinator = self.editCoordinator
                //            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(cellLongPressed))
                //            cell.addGestureRecognizer(longPress)
               // let dragDelegate = DragDelegate()
                cell.dragHandler = self
                // cell.dragDelegate = dragDelegate
                cells[key] = cell
            }
            
            
            
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        override func viewDidLoad() {
            setupSubViews()
            
            super.viewDidLoad()
            view.backgroundColor = UIColor.white
            let item = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(saveAndClose))
            let closeItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_clear"), style: .done, target: self, action: #selector(closeButtonPressed))
            self.navigationItem.setLeftBarButton(closeItem, animated: false)
            
            self.navigationItem.setRightBarButton(item, animated: false)
            
            self.navigationItem.setRightBarButton(item, animated: false)
            
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.navigationBar.layer.zPosition = -1;
            
            scanSimulation.startSimulation {
                self.navigationController?.navigationBar.layer.zPosition = 0
            }
        }
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        @objc private func saveAndClose() {
            
            assert(status == .none)
            
            let newDeviceContact = contact.makeDeviceContact()
            let deviceContact = DeviceContactController(for: newDeviceContact) // DeviceContactController(for: newDeviceContact)
            self.navigationController!.pushViewController(deviceContact, animated: true)
            
        }
        
        @objc private func closeButtonPressed() {
            self.dismiss(animated: true, completion: nil)
        }
        
        
        private func setupSubViews() {
            view.addSubview(self.contactTable)
            view.addSubview(self.gridView)
            self.navigationItem.titleView = self.segmentControl
            contactTable.translatesAutoresizingMaskIntoConstraints = false
            gridView.translatesAutoresizingMaskIntoConstraints = false
            contactTable.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
            contactTable.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            contactTable.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            
            contactTable.bottomAnchor.constraint(equalTo: gridView.topAnchor, constant: 0).isActive = true
            
            gridView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
            gridView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
            gridView.heightAnchor.constraint(equalToConstant: inputFieldHeight).isActive = true
            gridViewBottomAnchor = gridView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: inputFieldHeight)
            gridViewBottomAnchor.isActive = true
            
            self.view.addSubview(scanSimulation)
            view.bringSubview(toFront: scanSimulation)
            view.addConstraintsWithFormat(format: "V:|[v0]|", views: scanSimulation)
            view.addConstraintsWithFormat(format: "H:|[v0]|", views: scanSimulation)
        }
        
        @objc func segmentControlSelected(sender: UISegmentedControl) {
            if sender.selectedSegmentIndex == 0 {
                if status == .activeEdit {
                    editingCell?.forceFinishEditMode()
                    editingCell = nil
                }
                status = .none
                gridView.endEditing()
            } else {
                status = .correctionMode
            }
            contactTable.reloadData()
            
            
        }
        
        
        
        
        @objc func keyboardWillShow(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                self.gridViewBottomAnchor.constant = -keyboardSize.height
            }
        }
        
        @objc func keyboardWillHide(notification: NSNotification) {
            if let _ = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                self.gridViewBottomAnchor.constant = self.inputFieldHeight
            }
        }
        
        
        
        private var hoveringCell: CanHandleDrops?
        
        
    }
    
    
    
    
    extension ContactViewController: UITextFieldDelegate {
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if let inputTextField = textField as? InputTextField {
                inputTextField.returnPressed()
            }
            return true
        }
    }
    
    extension ContactViewController: UITableViewDataSource, UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return cells.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let row = indexPath.row
            if let key = contact.properties[row]?.0, let cell = cells[key] {
                if key != editingCell?.key {
                    cell.configure(self.status)
                }
                return cell
            }
            return ContactTableCell(textFieldDelegate: self)
            
            // return cells[key]!
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            print("cell selected")
        }
    }
    
    extension ContactViewController: ContactTableDelegate {
        func cellLongPressed() {
            if status == .none {
                segmentControl.selectedSegmentIndex = 1
                segmentControlSelected(sender: segmentControl)
            }
            // self.editButtonPressed()
        }
        
        
        func cellEnteredEditMode(cell: ContactTableCell) {
            
            gridView.isHidden = false
            gridViewBottomAnchor.constant = 0  // will be adjusted when keyboard is toggled
            
            if cell != self.editingCell {
                // only scroll there if
                
                editingCell?.forceFinishEditMode()
                self.editingCell = cell
                self.status = .activeEdit
                
                if let indexPath = contactTable.indexPath(for: cell) {
                    print("Scroll to Indexpath: \(indexPath)")
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
                        self.contactTable.scrollToRow(at: indexPath, at: .top, animated: true)
                    }
                }
            } else {
                fatalError("This function should only be triggered by cells in dragMode")
            }
            
            // inputField.update(text: text)
            gridView.startEditing()
        }
        
        func cellLeftEditMode() {
            self.inputListener = nil
            self.editingCell = nil
            self.status = .correctionMode
            gridView.isHidden = true
            gridView.endEditing()
            gridViewBottomAnchor.constant = inputFieldHeight
        }
        
        func cellHeightChanged() {
            contactTable.beginUpdates()
            contactTable.endUpdates()
        }
        
        
    }
    
    extension ContactViewController: DragDropHandler {
        func getStatus() -> ContactState {
            return self.status
        }
        func elementDragging(with gesture: UILongPressGestureRecognizer) {
            
            if status != .activeDrag {
                return
            }
            
            guard let element = gesture.view as? DragElement else {
                return
            }
            element.center = gesture.location(in: self.contactTable)
            
            if let cell = contactTable.cell(at: gesture.location(in: contactTable)) as? CanHandleDrops {
                cell.hoveringEvent(element: element)
                
                if cell.getId() != hoveringCell?.getId() {
                    hoveringCell?.unhover()
                    hoveringCell = cell
                }
                
            } else {
                // handles case when element is hovered over empty cell
                hoveringCell?.unhover()
                hoveringCell = nil
            }
            
            
            
        }
        
        func elementFinishedDragging(with gesture: UILongPressGestureRecognizer) {
            
            if status != .activeDrag {
                return
            }
            
            self.status = .correctionMode
            
            guard let element = gesture.view as? DragElement else {
                return
            }
            
            hoveringCell?.dropEvent(element: element)
            hoveringCell = nil
            
            
            for cell in cells.values {
                cell.leaveDragMode()
            }
            contactTable.reloadData()
            self.status = .correctionMode
        }
        
        func elementStartDragging(with gesture: UILongPressGestureRecognizer) {
            
            if status != .correctionMode {
                return
            }
            
            guard let element = gesture.view as? DragElement else {
                return
            }
            guard let cell = element.clippedToView() as? ContactTableCell else {
                return
            }
            
            self.status = .activeDrag
            
            for cell in cells.values {
                cell.enterDragMode()
            }
            //element.translatesAutoresizingMaskIntoConstraints = true
            print(element.frame)
            
            let frameBefore = element.frame
            
            let origin = contactTable.convert(frameBefore.origin, from: cell.scrollView)
            //let minY = contactTable.convert(frameBefore.minY, from: cell)
            
            contactTable.addSubview(element)
            
            print(element.frame)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                element.frame = CGRect(x: origin.x, y: origin.y, width: frameBefore.width, height: frameBefore.height)
                print(element.frame)
                UIView.animate(withDuration: 0.2) {
                    self.elementDragging(with: gesture) // to avoid element jump
                }
            }
            
        }
    }
    
    
    
