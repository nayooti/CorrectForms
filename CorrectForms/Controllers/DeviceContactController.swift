//
//  FinishedContactController.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 15.08.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit
import ContactsUI

class DeviceContactController: CNContactViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
//        let item = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(backButtonPressed))
        let item = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_clear"), style: .done, target: self, action: #selector(backButtonPressed))
        self.navigationItem.setLeftBarButton(item, animated: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func backButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
