//
//  PlainViewController.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 06.08.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit


class PlainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var client: APIClient
    var contact: Contact
    var imagePath: String?

    var tableViewBottomContraint: NSLayoutConstraint!
    
    lazy var scanSimulation: ScanSimulation = {
       let scanView = ScanSimulation()
        scanView.setupSubviews(imagePath: imagePath!)
        return scanView
    }()
    var tableView = UITableView()
    
    var propertyList: [Int:(String,String?)] = [:]
    
    init(client: APIClient, contact: Contact) {
        self.client = client
        self.contact = contact
        self.propertyList = contact.properties
        super.init(nibName: nil, bundle: nil)
        
        self.tableView.allowsSelection = false
        self.tableView.register(PlainCell.self, forCellReuseIdentifier: "plainCell")
        tableView.dataSource = self
        tableView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
  
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            tableViewBottomContraint.constant = -keyboardSize.height
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.navigationItem.leftBarButtonItem?.isEnabled = false

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableViewBottomContraint.constant = 0
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.navigationItem.leftBarButtonItem?.isEnabled = true

        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saveItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(saveAndClose))
        self.navigationItem.setRightBarButton(saveItem, animated: false)
        
        let closeItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_clear"), style: .done, target: self, action: #selector(closeButtonPressed))
        self.navigationItem.setLeftBarButton(closeItem, animated: false)
        
        self.title = "Edit Contact"
        
        
        self.view.addSubview(tableView)
        self.view.addSubview(scanSimulation)
        
        tableViewBottomContraint = tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        tableViewBottomContraint.isActive = true
        view.addConstraintsWithFormat(format: "V:|[v0]|", views: scanSimulation)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: scanSimulation)
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        // view.addConstraintsWithFormat(format: "V:|[v0]|", views: tableView)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: tableView)
        self.navigationController?.navigationBar.layer.zPosition = -1;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scanSimulation.startSimulation {
            self.navigationController?.navigationBar.layer.zPosition = 0
        }
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return contact.properties.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "plainCell", for: indexPath) as? PlainCell ?? PlainCell()
        
        cell.key = propertyList[indexPath.row]!.0
        cell.value = propertyList[indexPath.row]!.1
        cell.index = indexPath.row
        cell.plainControllerDelegate = self
        cell.textFieldDelegate = self
        return cell
    }
    
    @objc private func saveAndClose() {
        
        
        for property in propertyList.values {
            contact.update(key: property.0, value: property.1)
        }
        
        let newDeviceContact = contact.makeDeviceContact()
        let deviceContact = DeviceContactController(for: newDeviceContact) // DeviceContactController(for: newDeviceContact)
        self.navigationController!.pushViewController(deviceContact, animated: true)
    }
    
    @objc private func closeButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }

    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PlainViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    
}

extension PlainViewController: PlainControllerDelegate {
    func update(index: Int, key: String, value: String) {
        
        propertyList[index] = (key,value)
    }
    
}

protocol PlainControllerDelegate: class {
    func update(index: Int, key: String, value: String)
}
