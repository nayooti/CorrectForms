//
//  TutorialController.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 04.09.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

struct TutorialData {
    let filename: String
}

class TutorialDisplayController: UIViewController {
    
    var imageView: UIImageView = {
       let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    init(filename: String) {
        super.init(nibName: nil, bundle: nil)
        let img = UIImage(named: filename)
        imageView.image = img
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        view.backgroundColor = UIColor.white
    }
}

class TutorialController: UIPageViewController {

    var tutorialData: [TutorialData]
    
    var currentIndex: Int = 0
    
    var displayControllers: [TutorialDisplayController]
    
    /*
    lazy var pageViewController: TutorialPageViewController = {
        let pvc = TutorialPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pvc.delegate = self
        pvc.dataSource = self
        pvc.setViewControllers(displayControllers, direction: .forward, animated: true, completion: nil)
        return pvc
    }()
    */
    
    init(data: [TutorialData]) {
        self.tutorialData = data
        
        var array: [TutorialDisplayController] = []
        let _ = tutorialData.map({ array.append(TutorialDisplayController(filename: $0.filename))})
        displayControllers = array
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let skip = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(closeButtonPressed))
        navigationItem.setRightBarButton(skip, animated: false)
        
        navigationItem.hidesBackButton = true
        let back = UIBarButtonItem(title: "Zurück", style: .done, target: self, action: #selector(backButtonPressed))
        navigationItem.setLeftBarButton(back, animated: false)
        
        let appearance = UIPageControl.appearance(whenContainedInInstancesOf: [TutorialController.self]) //appearance(whenContainedInInstancesOf: TutorialPageViewController.self, nil)
        appearance.pageIndicatorTintColor = SystemColor.yellow.uiColor
        appearance.currentPageIndicatorTintColor = SystemColor.orange.uiColor
        
        dataSource = self
        delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let firstViewController = displayControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }
    
    
    @objc func backButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func closeButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    var pageIndex: Int = 0
}

extension TutorialController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            if pageIndex == displayControllers.count - 1 {
                let skip = UIBarButtonItem(title: "Fertig", style: .plain, target: self, action: #selector(closeButtonPressed))
                navigationItem.setRightBarButton(skip, animated: false)
            } else {
                let skip = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(closeButtonPressed))
                navigationItem.setRightBarButton(skip, animated: false)
            }
        }
  
        // otherwise, do nothing - that is, the animation did not
        // complete (someone suggested to set nextIndex = currentIndex, but
        // when delegates are called correctly, this should not be necessary)
    }
    
}

extension TutorialController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?  {

        guard let viewControllerIndex = displayControllers.index(of: viewController as! TutorialDisplayController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = displayControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
   
        
        pageIndex = nextIndex
        
        return displayControllers[nextIndex]

    }
    

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = displayControllers.index(of: viewController as! TutorialDisplayController) else {
            return nil
        }
    
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard displayControllers.count > previousIndex else {
            return nil
        }
        pageIndex = previousIndex
        return displayControllers[previousIndex]
        
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return displayControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex = displayControllers.index(of: firstViewController as! TutorialDisplayController) else {
            return 0
        }
        
        return firstViewControllerIndex
    }
}
