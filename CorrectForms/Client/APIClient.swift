//
//  APIClient.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 28.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import Foundation

class APIClient {
    
    private lazy var contacts: [Contact] = {
        return [def_drag,def_edit,pv_drag,pv_edit,sample, me]
    }()
    
    
    
    private lazy var def_drag: Contact = {
        let contact = Contact(Vorname: "Stephan", Nachname: "Mengs", Beruf: "Vertriebsleiter", Unternehmen: nil, Abteilung: nil, Straße: "Langewiesenerstraße 57", PLZ: "98693", Stadt: "Ilmenau", Land: "Deutschland", Telefonnummer: "01635234488", Mobil: "036776187535", Email: "s.mengs@mengsfenster.com", Webseite: "www.mengsfenster.de")
        return contact
    }()
    
    private lazy var def_edit: Contact = {
        let contact = Contact(Vorname: "Christoph", Nachname: "Lohmüller", Beruf: "Marketing", Unternehmen: "Lehmann Technologies AG", Abteilung: nil, Straße: "Wilhelmstra/%&$$e 22", PLZ: "17407", Stadt: "Heilbronn", Land: "IDeutschland", Telefonnummer: "4971315010022", Mobil: "4917233442", Email: "christoph.lohmüller@lehmanntec.de", Webseite: "www.lehmanntec.de")
        return contact
    }()
    
    private lazy var pv_drag: Contact = {
        let contact = Contact(Vorname: "Hektor", Nachname: "Vogts", Beruf: nil, Unternehmen: "Vogts Solutions GmbHI", Abteilung: nil, Straße: "Kupferstraße 26", PLZ: "85049", Stadt: "Ingolstadt", Land: "Deutschland", Telefonnummer: "017988210755", Mobil: "08414522660", Email: "hf@vsolutions.com", Webseite: "vsolutions.com")
        return contact
    }()
    
    
    private lazy var pv_edit: Contact = {
        let contact = Contact(Vorname: "Birte", Nachname: "Hansen geb. Heller", Beruf: "Gesch/%&$$ftsf/%&$$hrung", Unternehmen: "Ematec GmbH", Abteilung: nil, Straße: "Kotbusserdamm 401", PLZ: "10967", Stadt: "Berlin", Land: "Deutschland", Telefonnummer: "017845622877", Mobil: nil, Email: "hansen@ematec.de", Webseite: nil)
        //Contact(firstName: "Jesse", lastName: "Josua Benjamin", profession: "Researcher")
        return contact
    }()
    
    private lazy var sample: Contact = {
        let contact = Contact(Vorname: "John", Nachname: "Appleseed", Beruf: "Project Manager", Unternehmen: "Apple Inc", Abteilung: nil, Straße: "1 Infinite Loop", PLZ: "95014", Stadt: "Cupertino", Land: "CA-USA", Telefonnummer: "(408)555-0941", Mobil: nil, Email: "j.appleseed@icloud.com", Webseite: nil)
        //Contact(firstName: "Jesse", lastName: "Josua Benjamin", profession: "Researcher")
        return contact
    }()

	private lazy var me: Contact = {
		let contact = Contact(Vorname: "Bastian", Nachname: "van", Beruf: "ios-Developer", Unternehmen: "de Wetering", Abteilung: "Urbanstraße 27", Straße: "D-10967", PLZ: nil, Stadt: "Berlin", Land: "Germany", Telefonnummer: "01794631650", Mobil: "03075547825", Email: "b.wetering@gmail.com", Webseite: nil)
		//Contact(firstName: "Jesse", lastName: "Josua Benjamin", profession: "Researcher")
		return contact
	}()



    
    public func getContactList() -> [Contact] {
        return contacts
    }
    
    public func getContact(index: Int) -> Contact {
        if index > contacts.count - 1 {
            fatalError()
        }
        
        return contacts[index]
    }
    
    public func safe(contact: Contact) {
        
        print("safe to device")
        
        print(contact)
    
    }
    

    
    
    
}
