//
//  InputTextView.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 10.08.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class InputTextField: UITextField, CanHandleSelection {
    
    var surpressSelectionFowarding: Bool = false
    weak var editCoordinator: CanHandleTextFieldEvents?
    
    override var text: String? {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                if !self.surpressSelectionFowarding {
                    self.editCoordinator?.textViewRangeSelected(from: self.cursorStart, to: self.cursorEnd)
                }
            }
        }
    }
    
    override var selectedTextRange: UITextRange? {
        didSet {
            super.selectedTextRange = selectedTextRange
            if !surpressSelectionFowarding {
                editCoordinator?.textViewRangeSelected(from: cursorStart, to: cursorEnd)
            }
            surpressSelectionFowarding = false
        }
    }
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    
    private var cursorStart: Int? {
        guard let range = selectedTextRange else { return nil }
        return offset(from: beginningOfDocument, to: range.start)
    }
    
    private var cursorEnd: Int? {
        guard let range = selectedTextRange else { return nil }
        return offset(from: beginningOfDocument, to: range.end)
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.clearButtonMode = UITextFieldViewMode.whileEditing
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startEditSession(with text: String) {
        surpressSelectionFowarding = true
        self.text = text
    }
    
    func selectRange(from start: Int?, to end: Int?) {
        self.surpressSelectionFowarding = true
        if start == nil && end == nil {
            let newPosition = self.endOfDocument
            self.selectedTextRange = self.textRange(from: newPosition, to: newPosition)
            return
        } else {
            guard let start_ = start else {
                fatalError()
                return
            }
            
            let end_ = end ?? start_
            
            
            let startPosition = self.position(from: self.beginningOfDocument, offset: start_)
            let endPosition = self.position(from: self.beginningOfDocument, offset: end_)
            
            if startPosition != nil && endPosition != nil {
                self.selectedTextRange = self.textRange(from: startPosition!, to: endPosition!)
            } else {
                return
            }
        }
        
    }
    
//    func cancelEditing() {
//        self.text = editCoordinator.getBackupText()
//        editCoordinator?.textViewEditingFinished(with: backupText ?? "")
//    }
//    
//    func restoreEditing() {
//        self.text =  editCoordinator.getBackupText()
//        editCoordinator?.textViewUpdated(with: backupText ?? "")
//    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        editCoordinator?.textViewUpdated(with: textField.text ?? "")
        editCoordinator?.textViewRangeSelected(from: cursorStart, to: cursorEnd)
        
    }
    
    // triggered when delete-Button is pressed
    override func deleteBackward() {
        if self.text == "" {
            editCoordinator?.textViewDeleteEventOnEmptyField()
        } else {
            super.deleteBackward()
        }
    }
    
    public func returnPressed() {
        editCoordinator!.textViewEditingFinished(with: self.text ?? "", animated: true)
        self.endEditing(true)
        /*
         if let inputView = self.superview as? InputView {
         inputView.endEditing()
         }
         */
    }
    
    public func getText() -> String {
        return self.text ?? ""
    }
    
}

