//
//  EditCollection.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 29.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class GridCollection: UICollectionView {
    
    weak var editCoordinator: CanHandleGridEvents?
    
    
    private var value: String?
    private var chars: [Character] = [] {
        didSet {
         print("Chars set to \(chars)")
        }
    }
    private var itemsCount: Int = 0
    
    private var selectedRange: (Int?,Int?) = (nil,nil) {
        didSet {
            scrollToSelectedRange()
        }
    }
    

    
    private var layout:UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        //layout.itemSize = CGSize(width: 40, height: 40)
        layout.estimatedItemSize = CGSize(width: 4, height: Sizes.gridCharacter_Height)
        layout.sectionInset = UIEdgeInsets(top: 2, left: 0, bottom: 2, right: 0)
        layout.minimumLineSpacing = 3
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    
    init() {
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        self.register(GridCell.self, forCellWithReuseIdentifier: "GridCell")
        self.backgroundColor = UIColor.white 
        self.delegate = self
        self.dataSource = self
        self.showsHorizontalScrollIndicator = false 
        allowsMultipleSelection = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    public func getText() -> String {
        print("Current Value: \(self.value)")
        assert(self.value != "(function)")
        return self.value ?? ""
    }
    */
    
    public func update(text: String, proceedInput: Bool) {
        self.value = text
        self.chars = Array(text)
        itemsCount = chars.count * 2 + 1
        
        selectRange(from: nil, to: nil, proceedInput: proceedInput)
        reloadData()
        
        if proceedInput {
            editCoordinator?.gridUpdated(with: text)
        }
    }
    
    
    // index also take insert cells into account
    private func selectRange(from start: Int?, to end: Int?, proceedInput: Bool) {

        
        if proceedInput {
            var valueStartIndex:Int? = nil
            var valueEndIndex:Int? = nil

            if start != nil {
                valueStartIndex = start! / 2
                if start! % 2 == 1 && end == nil {
                    valueEndIndex = valueStartIndex! + 1
                }
            }
            if end != nil {
                if end! % 2 == 1 {
                    valueEndIndex = end! / 2 + 1
                } else {
                    valueEndIndex = end! / 2
                }
                
            }
            editCoordinator?.gridRangeSelected(from: valueStartIndex, to: valueEndIndex)
        }
        
        
        self.selectedRange = (start,end)
    }
}

extension GridCollection: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // "Hello" = +H+E+L+L+O+
//        var cellCount = chars.count * 2
//        return 1 + cellCount
        return itemsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCell", for: indexPath) as! GridCell
        cell.delegate = self
        let row = indexPath.row
        
        if row % 2 == 0 {
            cell.updateCell(type: .insert, value: nil, index: row)
        } else {
            cell.updateCell(type: .value, value: chars[row/2], index: row)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let row = indexPath.row
        if selectedRange.0 == nil && selectedRange.1 == nil  {
            selectRange(from: row, to: nil, proceedInput: true)
        }
        else if row % 2 == 0 {
            deselectAll(proceedInput: true)
            selectRange(from: row, to: nil, proceedInput: true)
        }
            
        else if (selectedRange.0 != nil && selectedRange.1 != nil) {
            deselectAll(proceedInput: true)
            selectRange(from: row, to: nil, proceedInput: true)
            
        } else {
            // one selectedIndex is set
            
            let indexSet = selectedRange.0 ?? selectedRange.1!
            
            // if selectedIndex is cursor, don not select range
            if indexSet % 2 == 0 {
                deselectAll(proceedInput: true)
                selectRange(from: row, to: nil, proceedInput: true)
            } else {
                
                let (min,max) = row < indexSet ? (row,indexSet) : (indexSet,row)
                
                for i in min...max {
                    collectionView.selectItem(at: IndexPath(row: i, section: 0), animated: false, scrollPosition: .centeredVertically)
                }
                selectRange(from: min, to: max, proceedInput: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        // reset
        deselectAll(proceedInput: true)
        // select again if item was within selected range
        if selectedRange.0 != nil && selectedRange.1 != nil {
            if selectedRange.0! <= indexPath.row && indexPath.row <= selectedRange.1! {
                selectItem(at: indexPath, animated: false, scrollPosition: .centeredVertically)
                selectRange(from: indexPath.row, to: nil, proceedInput: true)

                return
            }
        }
        selectRange(from: nil, to: nil, proceedInput: true)
    }
    
    func selectAll() {
        let itemsCount = collectionView(self, numberOfItemsInSection: 0)
        for i in 0..<itemsCount {
            self.selectItem(at: IndexPath(item: i, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition.centeredVertically)
        }
        selectRange(from: 0, to: itemsCount - 1, proceedInput: true)
    }
    
    private func deselectAll(proceedInput: Bool) {
        if selectedRange.0 == nil && selectedRange.1 == nil {
            return
        }
        
        let min:Int
        let max:Int
        
        if selectedRange.0 != nil && selectedRange.1 != nil {
            min = selectedRange.0!
            max = selectedRange.1!
            
            
            for i in min...max {
                deselectItem(at: IndexPath(row: i, section: 0), animated: false)
            }
        } else {
            let index = selectedRange.0 ?? selectedRange.1!
            deselectItem(at: IndexPath(row: index, section: 0), animated: false)
        }
        selectRange(from: nil, to: nil, proceedInput: proceedInput)
    }
    
    private func scrollToSelectedRange() {
        /*
        if selectedRange.0 != nil {
            if selectedRange.1 != nil {
                let midIndex = (selectedRange.0! + selectedRange.1!) / 2
                //scrollToItem(at: IndexPath(item: midIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
            } else {
                // selected item is last
                if selectedRange.0 == chars.count * 2 {
                    scrollToLastElement()
                    // scrollToItem(at: IndexPath(item: selectedRange.0!, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
                } else {
                // scrollToItem(at: IndexPath(item: selectedRange.0!, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
                }
            }
        }
        */
    }
    
    private func scrollToLastElement() {
        
        let itemsCount = collectionView(self, numberOfItemsInSection: 0)
        if itemsCount > 8 {
            let lastIndex = IndexPath(item: itemsCount - 1, section: 0)
            print(self.frame)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) { // change 2 to desired number of seconds
                self.scrollToItem(at: lastIndex, at: UICollectionViewScrollPosition.right, animated: false)
            }
        }
        
    }
}

extension GridCollection: GridCollectionDelegate {
    func cellDoubleTapped(cell: GridCell) {
        
        // it should select the insertCell left of the douple tapped cell
        if let indexPath = self.indexPath(for: cell) {
            deselectAll(proceedInput: false)
            
            if indexPath.row % 2 == 0 {
                selectItem(at: indexPath, animated: false, scrollPosition: .centeredVertically)
                selectRange(from: indexPath.row, to: nil, proceedInput: true)
                
            } else {
                selectItem(at: IndexPath(row: indexPath.row - 1, section: 0), animated: false, scrollPosition: .centeredVertically)
                selectRange(from: indexPath.row - 1, to: nil, proceedInput: true)
                
            }
        }
    }
    
    func cellLongPressed() {
        self.selectAll()
    }
    
    
}

extension GridCollection: CanHandleInput, CanHandleSelection {
    
    // indexes are without insertions
    func selectRange(from start: Int?, to end: Int?) {
        
        guard let start = start, let end = end else {
            return
        }
        
        deselectAll(proceedInput: false)
        if start == end {
            self.selectRange(from: start * 2, to: nil, proceedInput: false)
            selectItem(at: IndexPath(item: start * 2, section: 0), animated: true, scrollPosition: .centeredVertically)
        } else {
            let startIndex = start * 2 + 1
            let endIndex = end * 2 - 1
            self.selectRange(from: startIndex, to: endIndex, proceedInput: false)
            
            for i in startIndex...endIndex {
                selectItem(at: IndexPath(item: i, section: 0), animated: true, scrollPosition: .centeredVertically)
            }
        }
        scrollToSelectedRange()
    }
    
    func handle(range: NSRange, replacementString string: String) {
        
        let currentText = value ?? ""
        
        let  char = string.cString(using: .utf8)
        let charNum = strcmp(char, "\\b")
        
        print("char: \(char), charNum: \(charNum), range: \(range)")
        
        print("\(range.lowerBound):\(range.length)")
        
        // create IndexPaths that need to updated
        
        var selectedIndexPaths:[IndexPath] = []
        for i in 0..<range.length {
            selectedIndexPaths.append(IndexPath(row: (range.lowerBound + i) * 2, section: 0)) // letter
            selectedIndexPaths.append(IndexPath(row: (range.lowerBound + i) * 2 + 1, section: 0)) // insertElement
        }
        
        for path in selectedIndexPaths {
            if let cell = self.collectionView(self, cellForItemAt: path) as? GridCell {
                print(cell.value)
            }
        }
        
        
        // create range
        let currentTextRange = currentText.index(currentText.startIndex, offsetBy: range.lowerBound)..<currentText.index(currentText.startIndex, offsetBy: range.lowerBound + range.length)
        print("TextRange: \(currentTextRange)")
        
        switch charNum {
        case -92: // delete
            value?.removeSubrange(currentTextRange)
            self.performBatchUpdates( {
                self.deleteItems(at: selectedIndexPaths)
                itemsCount = itemsCount - selectedIndexPaths.count
            }, completion: nil)
        default:
            var insertIndexPaths:[IndexPath] = []
            for i in 0..<string.count {
                print("Insert Cells at Index \(range.lowerBound * 2 + 1)")
                
//                insertIndexPaths.append(IndexPath(item: 0, section: 0))
//                insertIndexPaths.append(IndexPath(item: 0, section: 0))
                insertIndexPaths.append(IndexPath(item: (range.lowerBound) * 2 + 1 , section: 0))
                insertIndexPaths.append(IndexPath(item: (range.lowerBound) * 2  + 1, section: 0))
            }
            
            value?.removeSubrange(currentTextRange)
            value?.insert(contentsOf: Substring(string), at: currentTextRange.lowerBound)
            self.performBatchUpdates( {
                
                self.deleteItems(at: selectedIndexPaths)
                insertItems(at: insertIndexPaths)
                itemsCount = itemsCount - selectedIndexPaths.count + insertIndexPaths.count
            }, completion: nil)
        }
        
    }
    
    func editingFinished(with text: String) {
        print("Editing finished")
    }
   
    func update(text: String) {
        self.update(text: text, proceedInput: false)
    }
}





enum ElementType {
    case value
    case insert
}

protocol GridCollectionDelegate: class {
    func cellLongPressed()
    func cellDoubleTapped(cell: GridCell)
}

