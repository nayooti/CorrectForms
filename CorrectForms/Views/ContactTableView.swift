//
//  ContactTableView.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 25.07.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

// TODO: probabyl not nessesary to subclass UITableView

class ContactTableView: UITableView {
    
    func cell(at position: CGPoint) -> UITableViewCell? {
        for cell in self.visibleCells {
            let cellFrame = convert(cell.frame, to: self)
            if cellFrame.contains(position) {
                return cell
            }
        }
        return nil
    }
    
    
}
