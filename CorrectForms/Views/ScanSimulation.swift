//
//  ScanSimulation.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 15.08.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class ScanSimulation: UIView {
    
    var scanLabel: UILabel = {
        let label = UILabel()
        label.text = "Scanning"
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 22)
        return label
    }()
    
    var dotLabel: UILabel = {
        let label = UILabel()
        label.text = "."
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 22)
        return label
    }()
    
    var line: UIView = {
        let line = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 2))
        line.backgroundColor = SystemColor.pink.uiColor
        return line
    }()
    
    var imageView: UIImageView = {
       let view = UIImageView()
//        let angle =  CGFloat(M_PI_2)
//        let tr = CGAffineTransform.identity.rotated(by: angle)
//        view.transform = tr
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    

    var lineTopAnchor: NSLayoutConstraint!
    
    
    func setupSubviews(imagePath: String) {
        self.backgroundColor = UIColor.darkGray
        
        addSubview(scanLabel)
        scanLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(dotLabel)
        dotLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(line)
        line.translatesAutoresizingMaskIntoConstraints = false
        
        if let image = UIImage(named: imagePath, in: nil, compatibleWith: nil) {
            addSubview(imageView)
            imageView.image = image
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5).isActive = true
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5).isActive = true
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        }
        
        line.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        lineTopAnchor = line.topAnchor.constraint(equalTo: topAnchor, constant: 60)
        lineTopAnchor.isActive = true
        line.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        line.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        scanLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -25).isActive = true
        scanLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        dotLabel.heightAnchor.constraint(equalTo: scanLabel.heightAnchor).isActive = true
        dotLabel.bottomAnchor.constraint(equalTo: scanLabel.bottomAnchor).isActive = true
        dotLabel.leadingAnchor.constraint(equalTo: scanLabel.trailingAnchor, constant: 4).isActive = true
    
        bringSubview(toFront: line)
    
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    private func stopSimulation() {
        scanLabel.text = "Processing Scan"
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.line.isHidden = true
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5, execute: {
            self.isHidden = true
            self.closeCallback?()
            self.timer?.invalidate()
        })
    }
    
    var timer: Timer?
    
    private func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(animate), userInfo: nil, repeats: true)

    }
    
    var lineTop = 60
    var closeCallback: (()->())?
    var down: Bool = true
    
    
    @objc private func animate() {
        let limit = Int(frame.maxY - 100)
        lineTop += 1
        
        if down {
            lineTopAnchor.constant = CGFloat(lineTop)
            if lineTop >= limit {
                down = false
                stopSimulation()
            }
            
        }
        
        
        
        
        
        
        if lineTop % 30 == 0 {
            
            
            if dotLabel.text == "" {
              dotLabel.text = "."
            } else if dotLabel.text == "." {
                dotLabel.text = ".."
            } else if dotLabel.text == ".." {
                dotLabel.text = "..."
            } else  {
                dotLabel.text = ""
            }
        }
    }
    
    
    func startSimulation(_ callback: @escaping ()->()) {
        self.isHidden = false
        self.closeCallback = callback
        startTimer()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.stopSimulation()
        })
    }

}
