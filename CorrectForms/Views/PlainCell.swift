//
//  PlainCell.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 15.08.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class PlainCell: UITableViewCell {
    
    var key: String? {
        didSet {
            self.keyLabel.text = key
        }
    }
    var value: String? {
        didSet {
            self.valueField.text = value
        }
    }
    
    var index: Int?
    
    var plainControllerDelegate: PlainControllerDelegate?
    var textFieldDelegate: UITextFieldDelegate? {
        didSet {
            valueField.delegate = textFieldDelegate
        }
    }
    
    lazy var keyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    lazy var valueField: UITextField = {
        let field = UITextField()
        field.borderStyle = .roundedRect
        field.autocorrectionType = .no
        field.addTarget(self, action: #selector(textEdited), for: UIControlEvents.editingChanged)
        field.backgroundColor = UIColor.init(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        return field
    }()
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        var containerView = UIView()
        
        contentView.addSubview(containerView)
        containerView.addSubview(keyLabel)
        containerView.addSubview(valueField)
        
        contentView.addConstraintsWithFormat(format: "H:|[v0]|", views: containerView)
        contentView.addConstraintsWithFormat(format: "V:|[v0]|", views: containerView)
        
        containerView.addConstraintsWithFormat(format: "H:|-16-[v0]|", views: keyLabel)
        containerView.addConstraintsWithFormat(format: "V:|-5-[v0(15)]-[v1(35)]-5-|", views: keyLabel,valueField)
        containerView.addConstraintsWithFormat(format: "H:|-10-[v0]-10-|", views: valueField)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func prepareForReuse() {
        self.plainControllerDelegate?.update(index: self.index!, key: self.key!, value: valueField.text ?? "")
    }
    
    
    @objc func textEdited() {
        self.plainControllerDelegate?.update(index: self.index!, key: self.key!, value: valueField.text ?? "")
    }
}
