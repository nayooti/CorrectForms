//
//  CharacterCell.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 02.07.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class GridCell: UICollectionViewCell {
    
    
    weak var delegate: GridCollectionDelegate?
    
    var type: ElementType?
    var value: Character?
    var index: Int!
    
    var labelWidth: NSLayoutConstraint!
    
    var label: EasyTouchLabel = {
        let label = EasyTouchLabel()
        label.backgroundColor = SystemColor.yellow.uiColor
        label.layer.cornerRadius = 2
        label.clipsToBounds = true
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 26)
        let tap = UITapGestureRecognizer(target: self, action: #selector(labelPressed))
        label.addGestureRecognizer(tap)
        return label
    }()
    
    override var isSelected: Bool {
        didSet {
            self.label.backgroundColor = isSelected ? SystemColor.orange.uiColor : SystemColor.yellow.uiColor
        }
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        contentView.addSubview(label)
        labelWidth = label.widthAnchor.constraint(equalToConstant: 22)
        labelWidth.isActive = true
        
        contentView.addConstraintsWithFormat(format: "H:|-2-[v0]|", views: label)
        contentView.addConstraintsWithFormat(format: "V:|-2-[v0(\(Sizes.gridCharacter_Height))]-2-|", views: label)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        self.contentView.addGestureRecognizer(longPress)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        doubleTap.numberOfTapsRequired = 2
        self.contentView.addGestureRecognizer(doubleTap)
        //self.makeBorder()
    }
    
    @objc private func longPressed() {
        delegate?.cellLongPressed()
    }
    
    @objc private func doubleTapped() {
        print("doubleTapp")
        delegate?.cellDoubleTapped(cell: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateCell(type: ElementType, value: Character?, index: Int) {
        assert((type == .value && value != nil) || (type == .insert && value == nil))
        self.index = index
        self.type = type
        self.value = value
        label.text = value != nil ? String(value!) : nil
        if type == .value {
            labelWidth.constant = Sizes.gridCharacter_Width
        } else {
            labelWidth.constant = 4
        }
    }
    
    @objc private func labelPressed() {
    }
    
    
    
    
}
