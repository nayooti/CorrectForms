//
//  InputView.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 02.07.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class GridView: UIView, UITextFieldDelegate {
    
    var editCoordinator: EditCoordinator
    
    private lazy var cancelButton: EasyTouchButton = {
        let button = EasyTouchButton()
        // button.setTitle("Cancel", for: .normal)
        button.setImage(#imageLiteral(resourceName: "ic_clear").withRenderingMode(.alwaysTemplate), for: .normal)
        style(button: button)
        button.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        return button
    }()
    
    
    private lazy var restoreButton: EasyTouchButton = {
        let button = EasyTouchButton()
        button.setImage(#imageLiteral(resourceName: "ic_settings_backup_restore").withRenderingMode(.alwaysTemplate), for: .normal)
        style(button: button)
        button.addTarget(self, action: #selector(restoreButtonPressed), for: .touchUpInside)
        return button
        
    }()
    
    
    private lazy var grid:GridCollection = {
        let collection = GridCollection()
        return collection
    }()
        
       
    
    init(editCoordinator: EditCoordinator) {
        self.editCoordinator = editCoordinator
        super.init(frame: CGRect.zero)
        editCoordinator.grid = grid
        self.grid.editCoordinator = editCoordinator
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = IOSColor.keyboard.uiColor
        
        addSubview(grid)
        addSubview(cancelButton)
        addSubview(restoreButton)
        //self.addConstraintsWithFormat(format: "H:|-[v0]-|", views: inputField)
         self.addConstraintsWithFormat(format: "H:|-6-[v0]-6-|", views: grid)
        
        // self.addConstraintsWithFormat(format: "H:|-6-[v0(26)]-6-[v1]-6-[v2(26)]-6-|", views: cancelButton, gridNew, restoreButton)
        self.addConstraintsWithFormat(format: "V:|-2-[v0]-2-|", views: grid)
        cancelButton.centerYAnchor.constraint(equalTo: grid.centerYAnchor, constant: 0).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: 26).isActive = true
        restoreButton.centerYAnchor.constraint(equalTo: grid.centerYAnchor, constant: 0).isActive = true
        restoreButton.heightAnchor.constraint(equalToConstant: 26).isActive = true
//        self.addConstraintsWithFormat(format: "V:|-[v0(24)]-|", views: cancelButton)
        //        self.addConstraintsWithFormat(format: "V:|-[v0(24)]-|", views: submitButton)
        
    }
    
    
    @objc private func cancelButtonPressed() {
        
    }
    
    private func style(button: UIButton) {
        button.backgroundColor = IOSColor.keyboardButton.uiColor
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.tintColor = UIColor.black
        button.layer.shadowColor = UIColor.darkGray.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 1)
        button.layer.shadowRadius = 0
        button.layer.shadowOpacity = 0.5
        button.layer.masksToBounds = false
    }
   
    @objc private func restoreButtonPressed() {
        
    }
    
    
    @objc private func doneButtonPressed() {
        // TODO:
        // editCoordinator?.textViewEditingFinished(with: inputField.text ?? "")
        
        self.endEditing()
    }
    
    
    func endEditing() {
      //  self.gridNew.endEditing(true)
    }
    
    func startEditing() {
      //  self.gridNew.becomeFirstResponder()
    }
    
    
            
}

