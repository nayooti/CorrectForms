//
//  ContactTableCell.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 28.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

enum ContactTableCellState {
    case none
    case correctionMode
    case editMode
    case editModeAll
    case dragMode
}

class ContactTableCell: UITableViewCell {
    
    public var key: String?
    public var value: String?
    
    weak var contactTableDelegate: ContactTableDelegate?
    // var dragDelegate: DragDelegate?
    weak var dragHandler: DragDropHandler?
    
    var editCoordinator: EditCoordinator!
    var textFieldDelegate: UITextFieldDelegate
    
    var proposalIndex: Int?
    
    private var showInsertButtons:Bool = true
    
    var state: ContactTableCellState {
        if !valueLabel.isHidden {
            return .none
        } else if !showInsertButtons {
            return .dragMode
        } else if editingElement != nil {
            return .editMode
        } else if dragElements.filter({ $0.state == .editMode }).count == (dragElements.count ) {
            return .editModeAll
        } else {
            return .correctionMode
        }
    }
    
    private var editingElement: DragElement?
    private var dragElements: [DragElement] = []
    
    private var keyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    private var valueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = SystemColor.blue.uiColor
        return label
    }()
    
    private var scrollViewContainer: UIView = {
        let container = UIView()
        return container
    }()
    
    public lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.white
        scrollView.addSubview(scrollViewContainer)
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
        scrollView.contentSize = scrollViewContainer.bounds.size // contentView.bounds.size
        let tap = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        scrollView.addGestureRecognizer(tap)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(scrollViewLongPress(_:)))
        scrollView.addGestureRecognizer(longPress)
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    public lazy var textView: InputTextField = {
        let textField = InputTextField()
        textField.delegate = self.textFieldDelegate
        textField.layer.cornerRadius = 4
        textField.autocorrectionType = .no
        textField.makeBorder(color: UIColor.lightGray, width: 0.8)
        textField.backgroundColor = UIColor.white
        //textField.isHidden = true
        return textField
    }()
    
    
    
    private var containerViewHeight: NSLayoutConstraint!
    private var editGridHeight: NSLayoutConstraint!
    
    init(textFieldDelegate: UITextFieldDelegate) {
        self.textFieldDelegate = textFieldDelegate
        super.init(style: .default, reuseIdentifier: nil)
        
        let containerView = UIView()
        
        contentView.addSubview(containerView)
        contentView.addConstraintsWithFormat(format: "H:|[v0]|", views: containerView)
        
        contentView.addConstraintsWithFormat(format: "V:|[v0]|", views: containerView)
        containerViewHeight = containerView.heightAnchor.constraint(equalToConstant: 50)
        containerViewHeight.priority = UILayoutPriority.init(900)
        containerViewHeight.isActive = true
        
        containerView.addSubview(keyLabel)
        containerView.addSubview(scrollView)
        containerView.addSubview(textView)
        
        containerView.addConstraintsWithFormat(format: "H:|-16-[v0]|", views: keyLabel)
        
        self.editGridHeight = textView.heightAnchor.constraint(equalToConstant: 0)
        editGridHeight.isActive = true
        
        
        containerView.addConstraintsWithFormat(format: "V:|-6-[v0(15)][v1(35)]-2-[v2]-5-|", views: keyLabel,scrollView,textView)
        containerView.addConstraintsWithFormat(format: "H:|-12-[v0]-|", views: textView)
        containerView.addConstraintsWithFormat(format: "H:|-[v0]-|", views: scrollView)
        
        scrollView.addConstraintsWithFormat(format: "H:|[v0]|", views: scrollViewContainer)
        scrollView.addConstraintsWithFormat(format: "V:|[v0]|", views: scrollViewContainer)
        
        scrollViewContainer.addSubview(valueLabel)
        scrollViewContainer.addConstraintsWithFormat(format: "H:|-[v0]|", views: valueLabel)
        scrollViewContainer.addConstraintsWithFormat(format: "V:|-8-[v0]|", views: valueLabel)
        
        // editView.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        keyLabel.text = "\(key!):"
        valueLabel.text = value
        textView.editCoordinator = editCoordinator
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        //        super.setSelected(selected, animated: animated)
    }
    
    public func configure(_ mode: ContactState) {
        switch mode {
        case .none:
            finishCorrectionMode()
            return
        case .correctionMode:
            showInsertButtons = true
        case .activeDrag:
            showInsertButtons = false
        case .activeEdit:
            showInsertButtons = true 
        }
        
        startCorrectionMode()
        
    }
    
    @objc private func scrollViewLongPress(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            if state == .none {
                print("longpress scrollview")
                contactTableDelegate?.cellLongPressed()
            }
        } else {
            return
        }
    }
    
    private func evaluate(elements: [DragElement]) -> String {
        
        if elements.count == 0 {
            return ""
        }
        return elements.map( { $0.title }).joined(separator: " ")
    }
    
    
    
    private func startCorrectionMode() {
        
        for element in dragElements {
            element.removeFromSuperview()
        }
        
        // make new Array
        self.dragElements = []
        
        // value could consist of many words
        if let elements = value?.split(separator: " ") {
            for element in elements {
                let dragElement = DragElement(title: String(element))
                dragElement.cellDelegate = self
                dragElement.dragHandler = self.dragHandler
                dragElements.append(dragElement)
            }
        }
        // last empty element to show insertButton at the end
        let lastEmptyElement = DragElement(title: "")
        lastEmptyElement.cellDelegate = self
        dragElements.append(lastEmptyElement)
        
        
        rearrangeDragElements()
        valueLabel.isHidden = true
        // editGrid.isHidden = true
        
    }
    // called from ContactViewController
    public func enterDragMode() {
        
        
        showInsertButtons = false
        
        for element in dragElements {
            element.hideInsertButton()
        }
    }
    // called from ContactViewController
    public func leaveDragMode() {
        
        showInsertButtons = true
        
        for element in dragElements {
            element.showInsertButton()
        }
    }
    
    private func finishCorrectionMode() {
        print(state)
        if state == .editMode || state == .editModeAll {
            finishEditMode()
        }
        for element in dragElements {
            element.removeFromSuperview()
        }
        valueLabel.text = evaluate(elements: self.dragElements)
        self.dragElements = []
        valueLabel.isHidden = false
    }
    
    private func rearrangeDragElements(animation: Bool = false) {
        
        if dragElements.count == 0 {
            return
        }
        
        // scrollViewContainer.removeConstraints(scrollViewContainer.constraints)
        
        var horizontalFormatString: String = "H:|"
        for (index,element) in dragElements.enumerated() {
            scrollViewContainer.addSubview(element)
            
            horizontalFormatString = "\(horizontalFormatString)-4-[v\(index)]"
            scrollViewContainer.addConstraintsWithFormat(format: "V:|-2-[v0]-2-|", views: element) //, options: [.alignAllCenterY])
        }
        horizontalFormatString = "\(horizontalFormatString)-|"
        scrollViewContainer.addConstraintsWithFormat(format: horizontalFormatString, views: dragElements)
        
        if animation {
            UIView.animate(withDuration: 0.4) {
                self.layoutIfNeeded()
            }
        }
    }
    
    @objc private func scrollViewTapped() {
        print("scrollview tapped")
        
        if self.state == .editModeAll || self.state == .editMode {
            finishEditMode()
        } else {
            let textTotal = evaluate(elements: dragElements)
            
            
            if state == .correctionMode {
                startEditMode(with: textTotal)
            }
            
            // just after this the current state will be interpreted as 'editModeAll'
            editingElement = nil
            
            for element in dragElements {
                element.update(state: .editMode)
            }
        }
    }
    
    
    private func dragElementPressed(_ dragElement: DragElement) {
        if state == .correctionMode {
            // enter editMode
            dragElement.update(state: .editMode)
            editingElement = dragElement
            startEditMode(with: dragElement.title)
        } else if state == .editModeAll {
            resetDragElementState(except: dragElement)
            editingElement = dragElement
            
            editCoordinator.update(with: dragElement.title)
            
            
        } else if state == .editMode {
            // cell already expanded
            
            switch dragElement.state {
            case .editMode:
                finishEditMode()
            case .insertMode, .readyForDeletionMode:
                // element internal switch
                dragElement.update(state: .editMode)
                editCoordinator.update(with: dragElement.title)
            case .none:
                guard let oldElement = editingElement else {
                    fatalError()
                    return
                }
                
                let text = textView.getText()
                
                switch oldElement.state {
                case .editMode:
                    if text.containsCharacters() {
                        oldElement.title = text
                    }
                case .insertMode:
                    break
                // ignore pending insert-Texts
                case .readyForDeletionMode:
                    // should stay the same
                    break
                case .none:
                    fatalError()
                }
                oldElement.update(state: .none)
                editingElement = dragElement
                dragElement.update(state: .editMode)
                editCoordinator.update(with: dragElement.title)
            }
        }
    }
    
    private func showGridView(text: String) {
        containerViewHeight.constant = 50
        editGridHeight.constant = 35
        textView.isHidden = false
        contactTableDelegate?.cellHeightChanged()
        
    }   
    
    private func hideGridView(forced: Bool = false, animated: Bool = true) {
        if animated {
            UIView.animate(withDuration: 0.4) {
                self.editGridHeight.constant = 0
                self.containerViewHeight.constant = 50
                
                self.textView.isHidden = true
                self.contactTableDelegate?.cellHeightChanged()
            }
            
        } else {
            self.editGridHeight.constant = 0
            self.containerViewHeight.constant = 50
            
            self.textView.isHidden = true
            self.contactTableDelegate?.cellHeightChanged()
        }
    }
    
    private func startEditMode(with text: String) {
        showGridView(text: text)
        editCoordinator.startEditSession(cell: self, with: text)
        contactTableDelegate!.cellEnteredEditMode(cell: self)
        self.textView.becomeFirstResponder()
    }
    
    
    private func finishEditMode() {
        
        forceFinishEditMode()
        contactTableDelegate?.cellLeftEditMode()
    }
    
    // called from contactTableView
    public func forceFinishEditMode() {
        
        if state == .editMode {
            guard let element = editingElement else {
                fatalError()
            }
            switch element.state {
            case .insertMode:
                let text = textView.getText()
                if text.containsCharacters() {
                    element.title = "\(text) \(element.title)"
                }
            case .editMode: element.title = textView.getText()
            default: break
            }
            self.value = evaluate(elements: self.dragElements)
        } else  if state == .editModeAll {
            self.value = textView.getText()
        } 
        startCorrectionMode()
        // resetDragElementState(except: nil)
        editingElement = nil
        textView.endEditing(true)
        hideGridView(forced: true)
        
    }
    
    private func resetDragElementState(except dragElement: DragElement?) {
        for element in dragElements {
            if element != dragElement {
                element.update(state: .none)
            }
        }
    }
}

extension ContactTableCell: EditSession {
    func editingFinished(with text: String, animated: Bool = false) {
        
        if state == .editMode {
            
            guard let element = editingElement else {
                fatalError()
            }
            switch element.state {
            case .insertMode:
                element.title = "\(text) \(element.title)"
            case .editMode:
                element.title = text
            default: break
            }
            self.value = evaluate(elements: self.dragElements)
        } else  if state == .editModeAll {
            self.value = text
        }
        
        
        self.editingElement = nil
        self.startCorrectionMode()
        self.contactTableDelegate?.cellLeftEditMode()
        self.hideGridView()
       
    }
    
    func handleDeleteEvent() {
        guard let editingElement = self.editingElement else {
            return
        }
        
        if editingElement.state == .editMode {
            editingElement.update(state: .readyForDeletionMode)
        } else if editingElement.state == .readyForDeletionMode {
            if let indexOfEditingElement = dragElements.index(of: editingElement) {
                dragElements.remove(at: indexOfEditingElement)
                editingElement.removeFromSuperview()
                self.value = evaluate(elements: dragElements)
                startCorrectionMode()
                
                if indexOfEditingElement > 0 {
                    let newEditingElement = dragElements[indexOfEditingElement - 1]
                    newEditingElement.update(state: .editMode)
                    editCoordinator.update(with: newEditingElement.title)
                    self.editingElement = newEditingElement
                    
                    
                } else {
                    self.finishEditMode()
                }
            }
            
            
        } else if editingElement.state == .insertMode {
            if let indexOfEditingElement = dragElements.index(of: editingElement) {
                editingElement.update(state: .none)
                
                if indexOfEditingElement > 0 {
                    let newEditingElement = dragElements[indexOfEditingElement - 1]
                    newEditingElement.update(state: .editMode)
                    editCoordinator.update(with: newEditingElement.title)
                    self.editingElement = newEditingElement
                    
                } else {
                    self.finishEditMode()
                }
            }
            
        } else if editingElement.state == .none {
            print("should never be none")
        }
    }
    
}


extension ContactTableCell: ContactTableCellDelegate {
    func clippedTo() -> UITableViewCell {
        return self
    }
    
    func unclipForDragging(_ dragElement: DragElement) {
        dragElement.cellDelegate = nil 
        if let index = dragElements.index(of: dragElement) {
            dragElements.remove(at: index)
            self.rearrangeDragElements(animation: true)
        }
        
        self.value = evaluate(elements: self.dragElements)
        
    }
    
    
    
    func handleEditEvent(_ dragElement: DragElement) {
        dragElementPressed(dragElement)
    }
    
    func handleInsertEvent(_ dragElement: DragElement) {
        
        if state == .correctionMode {
            // enter editMode
            dragElement.update(state: .insertMode)
            editingElement = dragElement
            startEditMode(with: "")
            
        } else if state == .editModeAll {
            // switch to editMode
            resetDragElementState(except: dragElement)
            dragElement.update(state: .insertMode)
            editingElement = dragElement
            editCoordinator.update(with: "")
        } else if state == .editMode {
            
            // cell already expanded
            switch dragElement.state {
            case .insertMode:
                // dragElement.update(state: .none)
                finishEditMode()
            case .readyForDeletionMode:
                dragElement.update(state: .insertMode)
                editCoordinator.update(with: "")
            case .editMode:
                assert(dragElement == editingElement)
                dragElement.title = textView.getText()
                dragElement.update(state: .insertMode)
                editCoordinator.update(with: "")
            case .none:    // cell internal switch
                guard let oldElement = editingElement else {
                    fatalError()
                    return
                }
                
                let text = textView.getText()
                
                switch oldElement.state {
                case .editMode:
                    if text.containsCharacters() {
                        oldElement.title = text
                    }
                case .insertMode:
                    break
                // ignore pending insert-Texts
                case .readyForDeletionMode:
                    // should stay the same
                    break
                case .none:
                    fatalError()
                }
                
                oldElement.update(state: .none)
                editingElement = dragElement
                dragElement.update(state: .insertMode)
                editCoordinator.update(with: "")
            }
        }
    }
}

extension ContactTableCell: CanHandleDrops {
    func getId() -> String {
        return self.key!
    }
    
    func unhover() {
        guard let propIndex = proposalIndex else {
            fatalError()
        }
        dragElements[propIndex].hideInsertButton()
        proposalIndex = nil
    }
    
    func hoveringEvent(element: UIView) {
        // find corresponding gap by Index
        let center = self.convert(element.center, from: element.superview)
        
        var hoveringIndex = 0
        
        if dragElements.count <= 1 {
            // is true for empty cells or the source cell
            hoveringIndex = 0
        } else {
            
            for dragElement in dragElements {
                if dragElement.center.x > center.x {
                    break
                } else {
                    hoveringIndex += 1
                    if hoveringIndex == dragElements.count - 1 {
                        break
                    }
                }
            }
        }
        
        
        if proposalIndex != hoveringIndex {
            if proposalIndex != nil {
                dragElements[proposalIndex!].hideInsertButton()
                dragElements[proposalIndex!].update(state: .none)
            }
            dragElements[hoveringIndex].showInsertButton()
            dragElements[hoveringIndex].update(state: .insertMode)
            proposalIndex = hoveringIndex
        }
        
        
        print(hoveringIndex)
    }
    
    func dropEvent(element: UIView) {
        guard let propIndex = proposalIndex, let dragElement = element as? DragElement else {
            fatalError()
        }
        dragElement.cellDelegate = self
        
        dragElements.insert(dragElement, at: propIndex)
        self.value = evaluate(elements: dragElements)
        proposalIndex = nil
        rearrangeDragElements(animation: true)
    }
    
    
}

