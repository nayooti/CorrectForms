//
//  DragElement.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 28.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

enum DragElementState {
    case insertMode
    case editMode
    case readyForDeletionMode
    case none
}


class DragElement: UIView, Draggable{

    
    weak var cellDelegate: ContactTableCellDelegate?
    weak var dragHandler: DragDropHandler?
    
    var title: String {
        didSet {
            if title == "" {
                mainView.isHidden = true
            } else {
                mainView.isHidden = false 

            label.text = title 
            }
        }
    }
    
    var state: DragElementState = .none
    
    
    lazy var mainView: UIView = {
        let view = UIView()
        view.backgroundColor = SystemColor.tealBlue.uiColor
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        var tap = UITapGestureRecognizer(target: self, action: #selector(labelPressed))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.text = title
        label.textColor = UIColor.white
        return label
    }()
    
    lazy var insertButton: UIButton = {
        let button = EasyTouchButton()
        button.setImage(#imageLiteral(resourceName: "ic_add").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = SystemColor.tealBlue.uiColor
        button.backgroundColor = UIColor.white
        button.makeBorder(color: SystemColor.tealBlue.uiColor)
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(insertButtonPressed), for: .touchUpInside)
        return button
    }()
    
    
    init(title: String) {
        self.title = title
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var insertButtonWidth:NSLayoutConstraint!
    
    
    override func layoutSubviews() {
        
        self.addSubview(mainView)
        mainView.addSubview(label)
        self.addSubview(insertButton)
        addConstraintsWithFormat(format: "H:|[v0]-4-[v1]|", views: insertButton, mainView)
        addConstraintsWithFormat(format: "V:|[v0(30)]|", views: mainView)
        insertButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        insertButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        insertButtonWidth = insertButton.widthAnchor.constraint(equalToConstant: 20)
        insertButtonWidth.isActive = true
        if self.title != "" {
        
        mainView.addConstraintsWithFormat(format: "H:|-[v0]-|", views: label)
        mainView.addConstraintsWithFormat(format: "V:|[v0]|", views: label)
        } else {
            mainView.isHidden = true
        }
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        longPress.minimumPressDuration = 0.2
        self.addGestureRecognizer(longPress)
        
    }
    @objc private func insertButtonPressed() {
        cellDelegate?.handleInsertEvent(self)
    }
    
    @objc private func labelPressed() {
        cellDelegate?.handleEditEvent(self)
    }
    
    func update(state: DragElementState) {
    
        mainView.backgroundColor = SystemColor.tealBlue.uiColor
        mainView.deleteBorder()
        insertButton.makeBorder(color: SystemColor.tealBlue.uiColor)    
        insertButton.tintColor = SystemColor.tealBlue.uiColor
        
        switch state {
        case .editMode: mainView.makeBorder(color: SystemColor.blue.uiColor)
        case .insertMode:
            insertButton.makeBorder(color: SystemColor.blue.uiColor)
            insertButton.tintColor = SystemColor.blue.uiColor
        case .readyForDeletionMode: mainView.backgroundColor = SystemColor.blue.uiColor
        case .none: break
        }
        
        self.state = state
    }
    
    func showInsertButton() {
        self.insertButton.isHidden = false
    }
    
    func hideInsertButton() {
      self.insertButton.isHidden = true
    }
    
    func clippedToView() -> UIView? {
        return cellDelegate?.clippedTo() 
    }
    
    var allowDragging: Bool = false
    
    @objc func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
    
        
        switch gesture.state {
        case .began:
            //superview?.layoutIfNeeded()
            let draggingEnabled = grab(with: gesture)
            if !draggingEnabled {
                allowDragging = false
                return
            }
            allowDragging = true
            cellDelegate?.unclipForDragging(self)
            UIView.animate(withDuration: 0.2) {
                self.insertButtonWidth.constant = 0
                self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.alpha = 0.8
            }
            
            
        case .changed:
            if allowDragging {
                move(with: gesture)
            }
        case .ended:
            // self.transform = CGAffineTransform.identity
            
            if allowDragging {
                drop(with: gesture)
                insertButtonWidth.constant = 20
            
            UIView.animate(withDuration: 0.2) {
                
                self.transform = CGAffineTransform.identity
                self.alpha = 1.0
            }
            }
        case .cancelled:
            // self.transform = CGAffineTransform.identity
            print("cancelled")
        case .failed:
            // self.transform = CGAffineTransform.identity
            print("failed")
        case .possible: print("possible")
        }
    }
    
    
    func grab(with gesture: UILongPressGestureRecognizer) -> Bool {
        
        guard let mvc = dragHandler else {
            return false
        }
        if mvc.getStatus() != .correctionMode {
            return false
        }
        dragHandler?.elementStartDragging(with: gesture)
        return true
    }
    
    func move(with gesture: UILongPressGestureRecognizer) {
        dragHandler?.elementDragging(with: gesture)
    }
    
    func drop(with gesture: UILongPressGestureRecognizer) {
        dragHandler?.elementFinishedDragging(with: gesture)
    }
    
}



