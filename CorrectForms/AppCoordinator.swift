//
//  AppCoordinator.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 28.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    private init() {}
    static var shared = AppCoordinator()
   
    
    var launcher: LaunchViewController = {
        let launcher = LaunchViewController()
        launcher.client = APIClient()
        return launcher
    }()
    
    /*
    var contactViewController: ContactViewController = {
        let contactViewController = ContactViewController(client: APIClient())
        return contactViewController
    }()
    */
    
    /*
    func getLaunchViewController() -> LaunchViewController {
        return self.launcher
    }
    */
    
    
    /*

    var plainViewController: PlainViewController = {
        let plainViewController = PlainViewController(client: APIClient())
        return plainViewController
    }()
    
    var launcher: LaunchViewController = {
        let launcher = LaunchViewController()
        launcher.client = APIClient()
        return launcher
    }()
    
    
    func getLaunchViewController() -> LaunchViewController {
        return self.launcher
    }
    
    func getContactViewController() -> ContactViewController {
        return self.contactViewController
    }
    
    func getPlainViewController() -> PlainViewController {
        return self.plainViewController
    }
 */
    /*
    func makeDropDelegate() -> DragDelegate {
        let dragDelegate = DragDelegate()
        dragDelegate.mainViewController = contactViewController
        return dragDelegate
    }
 */
 
}



