//
//  Protocols.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 28.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

protocol ContactTableDelegate: class {
    func cellLongPressed() 
    func cellHeightChanged()
    func cellEnteredEditMode(cell: ContactTableCell)
    func cellLeftEditMode() // used to close inputField
}

protocol EditSession: class {
    func editingFinished(with text: String, animated: Bool)
    func handleDeleteEvent()
}

protocol CanHandleSelection: class {
    func selectRange(from start: Int?, to end: Int?)
    // func selectRange(source: EventSource, from start: Int?, to end: Int?)
}

protocol CanHandleInput: class {
    func update(text: String)
    func editingFinished(with text: String)
    func handle(range: NSRange, replacementString string: String)
}

protocol CanHandleGridEvents: class {
    func gridUpdated(with text: String)
    func gridRangeSelected(from start: Int?, to end: Int?)
}

// interface provided from editController to textField
protocol CanHandleTextFieldEvents: class {
    func textViewRangeSelected(from start: Int?, to end: Int?)
    func textViewUpdated(with text: String)
    func handleInput(range: NSRange, replacementString string: String)
    func textViewEditingFinished(with text: String, animated: Bool)
    func textViewDeleteEventOnEmptyField()
    func getBackupText() -> String
}

protocol ContactTableCellDelegate: class {
    func handleInsertEvent(_ dragElement: DragElement)
    func handleEditEvent(_ dragElement: DragElement)
    func clippedTo() -> UITableViewCell
    func unclipForDragging(_ dragElement: DragElement)
//    func grab(_ dragElement: DragElement)
//    func drop(_ dragElement: DragElement)
}

protocol CanHandleDrops {
    func hoveringEvent(element: UIView)
    func dropEvent(element: UIView)
    func unhover()
    func getId() -> String 
}

protocol DragDropHandler: class {
    func elementStartDragging(with gesture: UILongPressGestureRecognizer)
    func elementDragging(with gesture: UILongPressGestureRecognizer)
    func elementFinishedDragging(with gesture: UILongPressGestureRecognizer)
    func getStatus() -> ContactState
}

protocol Draggable {
    func drop(with gesture: UILongPressGestureRecognizer)
    func grab(with gesture: UILongPressGestureRecognizer) -> Bool
    func move(with gesture: UILongPressGestureRecognizer)
}



