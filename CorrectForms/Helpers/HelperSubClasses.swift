//
//  HelperSubClasses.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 25.07.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

class EasyTouchButton: UIButton {
    var addedTouchArea = CGFloat(15)
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let newBound = CGRect(
            x: self.bounds.origin.x - addedTouchArea,
            y: self.bounds.origin.y - addedTouchArea,
            width: self.bounds.width + 2 * addedTouchArea,
            height: self.bounds.width + 2 * addedTouchArea
        )
        return newBound.contains(point)
    }
}

class EasyTouchLabel: UILabel {
    var addedTouchArea = CGFloat(15)
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let newBound = CGRect(
            x: self.bounds.origin.x - addedTouchArea,
            y: self.bounds.origin.y - addedTouchArea,
            width: self.bounds.width + 2 * addedTouchArea,
            height: self.bounds.width + 2 * addedTouchArea
        )
        return newBound.contains(point)
    }
}

