//
//  Enums.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 14.07.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import Foundation

enum Direction {
    case top
    case bottom
    case left
    case right 
}

enum EventSource {
    case Grid
    case TextField
}
