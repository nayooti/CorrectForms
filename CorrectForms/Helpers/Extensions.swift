//
//  Extensions.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 28.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit



extension UIView {
   
    func addConstraintsWithFormat(format: String, views: [UIView], options: NSLayoutFormatOptions = NSLayoutFormatOptions()) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: options, metrics: nil, views: viewsDictionary))
    }
    
    func addConstraintsWithFormat(format: String, views: UIView..., options: NSLayoutFormatOptions = NSLayoutFormatOptions()) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        // let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: options, metrics: nil, views: viewsDictionary)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: options, metrics: nil, views: viewsDictionary))
    }
    
    func makeBorder(color: UIColor = UIColor.red, width: CGFloat = 2) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
  
    /*
    func addTopBorderWithColor(color: UIColor = UIColor.lightGray, width: CGFloat = 0.5) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    */
    
    func makeBorder(edge: Direction? = nil, color: UIColor = UIColor.lightGray, width: CGFloat = 0.5) {
        guard let edge = edge else {
            makeBorder(color: color, width: width)
            return
        }
        
        let tempLayer = self.layer
        let b = tempLayer.bounds
        print(b)
        print(bounds)
        let border = CALayer()
        
        border.backgroundColor = color.cgColor
        
        
        switch edge {
        case .bottom:
            border.frame = CGRect(x: 0, y: bounds.width - width, width: bounds.width, height: width)
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: bounds.width, height: width)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: width , height: bounds.height)
        case .right:
            border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: bounds.height)
        }
        self.layer.addSublayer(border)
 
    }

    
    
    
    func deleteBorder() {
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
}

extension UIColor {
    static func rgb(_ red: CGFloat,_ green: CGFloat,_ blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}

extension String {
    func containsCharacters() -> Bool {
        return self.trimmingCharacters(in: [" "]).count > 0
    }
}




//extension String {
//    
//    func charAt(_ i: Int) -> Character {
//        return self[index(startIndex, offsetBy: i)]
//    }
//}



