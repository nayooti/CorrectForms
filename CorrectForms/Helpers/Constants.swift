//
//  Constants.swift
//  CorrectForms
//
//  Created by Bastian van de Wetering on 28.06.18.
//  Copyright © 2018 Bastian van de Wetering. All rights reserved.
//

import UIKit

enum SystemColor {
    
    case red
    case orange
    case yellow
    case green
    case tealBlue
    case blue
    case purple
    case pink
    
    var uiColor: UIColor {
        switch self {
        case .red:
            return UIColor(red: 255/255, green: 59/255, blue: 48/255, alpha: 1)
        case .orange:
            return UIColor(red: 255/255, green: 149/255, blue: 0/255, alpha: 1)
        case .yellow:
            return UIColor(red: 255/255, green: 204/255, blue: 0/255, alpha: 1)
        case .green:
            return UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        case .tealBlue:
            return UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 1)
        case .blue:
            return UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        case .purple:
            return UIColor(red: 88/255, green: 86/255, blue: 214/255, alpha: 1)
        case .pink:
            return UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
        }
    }
}

enum IOSColor {
    case keyboard
    case keyboardButton
    
    var uiColor: UIColor {
        switch self {
        case .keyboard: return UIColor.rgb(204,207,214)
        case .keyboardButton: return UIColor.rgb(164, 170, 182)
        }
    }
    
}

class Sizes {
    static let gridCollectionHeight: CGFloat = Sizes.gridCharacter_Height + 10
    static let gridCharacter_Width: CGFloat = 28
    static let gridCharacter_Height: CGFloat = 40

    
    
}
